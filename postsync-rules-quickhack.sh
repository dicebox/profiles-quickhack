#!/bin/bash
# Copyright © 2017 Julien Cerqueira (dicebox in #gentoo-hardened on freenode.net)

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

# Created the 2017/08/29
# THIS IS A PROOF OF CONCEPT, draft quality, based on an other POF, rules-quickhack.sh, from Julien Cerqueira too

# We propose to solve the difficulty of creating new profiles based on existing ones and of mixing profiles by creating a symmetric dirtree of the current profiles
# One dirtree would hold the relations between the nodes of the logical tree, so containing only the parent files
# The second dirtree would contain everything else used directly by the profiles
# This permit to call any profile's data, any node of the logical tree, without automatically calling its branch
# Concretely, among other possibilities it permits to create a hardened desktop profile, a hardened systemd profile, or even a hardened systemd desktop profile,
# with their corresponding selinux part, with great ease
# The creation of such profiles would rely only on the creation of a parent file with the right paths in it
# adding a corresponding line in profiles.desc and selecting this custom profile with eselect profile set
# you can find some proposals for hardened desktop profile, a hardened systemd profile, or even a hardened systemd desktop profile,
# with their corresponding selinux part at the end of this document
# The objective is to get the idea adopted by the gentoo devs, to make the change directly on the sync master servers (or whereever it should occur),
# only one time, for everyone
# this script permits to test and live with this idea in between the time of its adoption
# Obviously, a lot remain to be discussed and defined, like how to call that symmetric dirtree which i called rules-quickhack
# I am convinced that this is a needed step toward a more complex redesign of the current profiles system

# Few design decisions:
# we keep the parent files in place, so profilres.desc and current profiles do not break, which means it is transparent for gentoo users
# we rewrite the parents file in a way which does not solve the problem of profiles stacking for current profiles,
# so if you build a custom profile use the corresponding dis in /path/to/rules-quickhack

# proposed python sample by blueness on his blog, to get the current profile stack, to see other profiles stack, just use eselect profile set before
# #!/usr/bin/python
# import portage
# for p in portage.settings.profiles:
#     print("%s" % p)

# relative paths coud be used as well in the next two examples, i use absolute paths on my machine

# proposed parent file for hardened systemd
# /usr/portage/profiles/rules-quickhack/base
# /usr/portage/profiles/rules-quickhack/default/linux
# /usr/portage/profiles/rules-quickhack/arch/base
# /usr/portage/profiles/rules-quickhack/features/multilib
# /usr/portage/profiles/rules-quickhack/arch/amd64
# /usr/portage/profiles/rules-quickhack/releases
# /usr/portage/profiles/rules-quickhack/releases/13.0
# /usr/portage/profiles/rules-quickhack/targets/systemd
# /usr/portage/profiles/rules-quickhack/hardened/linux
# /usr/portage/profiles/rules-quickhack/hardened/linux/amd64

# proposed parent file for hardened systemd desktop, i reverted back to a simpler hardened systemd as adding desktop pulled too many dependancies for my taste and use
# /usr/portage/profiles/rules-quickhack/base
# /usr/portage/profiles/rules-quickhack/default/linux
# /usr/portage/profiles/rules-quickhack/arch/base
# /usr/portage/profiles/rules-quickhack/features/multilib
# /usr/portage/profiles/rules-quickhack/arch/amd64
# /usr/portage/profiles/rules-quickhack/releases
# /usr/portage/profiles/rules-quickhack/releases/13.0
# /usr/portage/profiles/rules-quickhack/targets/desktop
# /usr/portage/profiles/rules-quickhack/targets/systemd
# /usr/portage/profiles/rules-quickhack/hardened/linux
# /usr/portage/profiles/rules-quickhack/hardened/linux/amd64

# BUG?: if you use a custom profile in /etc/portage/profile-quickhack as this script permits, emerge/portage will complain that /etc/portage/make.profile does not point to any profile,
# just before running this script which solves the problem.

# HOWTO USE
# ONE TIME USE
# just exec the script, from anywhere
# AUTOMAGIC
# drop the script in /etc/portage/repo.postsync.d/

repository_name=${1}
sync_uri=${2}
repository_path=${3}
ret=0

if [[ "${repository_name}" != "gentoo" ]];then
  exit "${ret}"
fi

# TODO use portage variables if they are accessible, for /usr/portage/profiles for example

# list of targeted dirs in /usr/portage/profiles
# they do not necessarily contain parent files, but data directly used to build the profiles
targeted_dirs="arch base default embedded features hardened prefix releases targets uclibc"

if [[ ! -d "/usr/portage/profiles/rules-quickhack" ]];then
  mkdir /usr/portage/profiles/rules-quickhack
else
  echo "postsync-rules-quickhack.sh has already been executed or profiles tree is in an inconsistent state"
  echo "please resync your profiles tree"
  exit "-1"
fi

# lets first put the profiles tree in the expected state
for d in ${targeted_dirs}; do
  # copy targeted dirs in /usr/portage/profiles/rules-quickhack
  # populating the symmetric tree, we do the cleanup later
  cp -a /usr/portage/profiles/${d} /usr/portage/profiles/rules-quickhack
  # keep only parent files in targeted dirs, rm empty dirs the bruteforce way sorry
  # cleaning up unneeded remaining mess, as it has been moved previously
  # symmetric cleaning in the original tree, where we want to keep only the parent files
  # only the relations between the nodes
  targeted_files="$(find /usr/portage/profiles/${d} -type f)"
  for f in ${targeted_files}; do
    if [[ "$(basename ${f})" != "parent" ]]; then
      rm -f ${f}
    fi
  done
  # lets just rely on rmdir to tell us if the dir was empty or not. not sure that is nice
  dirs_to_clean="$(find /usr/portage/profiles/${d} -type d)"
  cleaned_dirs="$(for dtc in ${dirs_to_clean};do rmdir --ignore-fail-on-non-empty ${dtc};done)"
  while [[ "${cleaned_dirs}" != "" ]] && [[ -d "/usr/portage/profiles/${d}" ]]; do
    dirs_to_clean="$(find /usr/portage/profiles/${d} -type d)"
    cleaned_dirs="$(for dtc in ${dirs_to_clean};do rmdir --ignore-fail-on-non-empty ${dtc};done)"
  done
done
# rm parent files in /usr/portage/profiles/rules-quickhack
# we split the relation between the nodes of the tree from(and?) the node themselves
# by cleaning the symmetric tree
find /usr/portage/profiles/rules-quickhack -type f -name "*parent" -exec rm -f {} \;


purge_targeted_dirs=""
for d in ${targeted_dirs}; do
  if [[ -d "/usr/portage/profiles/${d}" ]];then
    purge_targeted_dirs="${purge_targeted_dirs} ${d}"
  fi
done

# edit each parent file, now that the trees should be in the right state
for d in ${purge_targeted_dirs}; do
  targeted_files="$(find /usr/portage/profiles/${d} -type f)"
  for f in ${targeted_files}; do
    if [[ "$(basename ${f})" == "parent" ]]; then
      # we are going to work with relative paths, so lets jump to the parent file directory
      cd "$(dirname ${f})"
      cp "${f}" "${f}.backup"
      rm -f "${f}"
      touch "${f}"
      # test each path, if dir does not exist, it means we encounter a leaf, then add change path to the corresponding dir in /usr/portage/profiles/rules-quickhack
      for p in $(cat ${f}.backup); do
        if [[ ! -d "${p}" ]];then
          p_path="$(readlink -m ${p})"
          p_relativepath="${p_path#/usr/portage/profiles}"
          p_targetdir="/usr/portage/profiles/rules-quickhack${p_relativepath}"
          # if corresponding dir in /usr/portage/profiles/rules-quickhack does not exist, print an error
          if [[ ! -d "${p_targetdir}" ]];then
            echo "target dir does not exist"
            exit "-1"
          fi
          echo "${p_targetdir}" >> ${f}
        else
        echo "${p}" >> ${f}
        fi
      done
      # add a link to the corresponding dir in /usr/portage/profiles/rules-quickhack of the current parent file being edited
      f_path="$(dirname ${f})"
      f_relativepath="${f_path#/usr/portage/profiles}"
      f_targetdir="/usr/portage/profiles/rules-quickhack${f_relativepath}"
      echo "${f_targetdir}" >> ${f}
      # anything else?
    else # at that point, there should remain no other file
      echo "ghost file, fallback"
      exit "-1"
    fi
  done
done

if [[ -f "/etc/portage/profile-quickhack/parent" ]] && [[ -f "/etc/portage/profile-quickhack/profiles.desc" ]];then
  mkdir /usr/portage/profiles/profile-quickhack
  ln -s /etc/portage/profile-quickhack/parent /usr/portage/profiles/profile-quickhack
  cat /etc/portage/profile-quickhack/profiles.desc >> /usr/portage/profiles/profiles.desc
fi

exit "${ret}"


